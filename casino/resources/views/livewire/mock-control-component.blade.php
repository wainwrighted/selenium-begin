<x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight dark:text-white">
            {{ __('Mock Panel') }}
        </h2>
    </x-slot>
        <div class="max-w-7xl dark:bg-gray-900 dark:text-white mx-auto py-10 sm:px-6 lg:px-8">   

        

        <div class="flex p-4 mb-4 text-sm text-blue-700 bg-blue-100 rounded-lg" role="alert">
            <svg aria-hidden="true" class="flex-shrink-0 inline w-5 h-5 mr-3" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
            <span class="sr-only">Info</span>
            <div>
                <span class="font-medium"><p>What we try to replicate with mocking module is in & outward callbacks as normally would to a casino. Casino then in turn performs balance modification and returns the new player balance for us to display within the game itself. Read <b><u>documentation</u></b> for more indepth details.</p>
            </div>
            
        </div>


        <a href="{{ env('APP_URL') }}/api/respins.io/aggregation/createSession?game=softswiss:AlohaKingElvis&currency=USD&mode=real&player=dejan&operator_key=96e9f726-74e3-4752-a0ca-7bcc758d7f43" target="_blank" class="inline-flex items-center text-xs bg-gray-700 text-gray-400 border-0 transition ease duration-300 mb-4 py-2 px-4 focus:outline-none hover:bg-gray-800 hover:text-gray-100 rounded-full mt-4 md:mt-0">
                  <p>Example (replace game, operator key accordingly):</p>
                  <br><br>
                  <p><i>/api/respins.io/aggregation/createSession?game=softswiss:AlohaKingElvis&currency=USD&mode=real&player=dejan&operator_key=96e9f726-74e3-4752-a0ca-7bcc758d7f43</i></p>
        </a>
        <div class="mt-10 mb-10"></div>
            <livewire:operatorkeys-datatable />
        </div>
    </div>