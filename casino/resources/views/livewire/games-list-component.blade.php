<x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight dark:text-white">
            {{ __('Games List') }}
        </h2>
    </x-slot>
<div class="max-w-full dark:bg-gray-900 mx-auto py-4 px-4 sm:px-8 md:px-10 lg:px-12">      

    <livewire:gameslist-datatable />
    
</div>
