@if($error === 1)
  <div class="container mx-auto px-4 py-24">
        @if (session()->has('error'))
            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 mb-2 rounded relative" role="alert">
              <strong class="font-bold">Something weird happened..</strong>
              <span class="block sm:inline">{{ session('error') }}</span>
            </div>
             <a href="/game-api/" class="hover:bg-gray-100 text-gray-100 hover:text-gray-800 font-medium text-xs py-2 px-4 border border-gray-400 rounded-full shadow cursor-pointer">Gameslist</a>
             <a class="hover:bg-gray-100 text-gray-100 hover:text-gray-800 font-medium text-xs py-2 px-4 border border-gray-400 rounded-full shadow cursor-pointer" href="/full-whitelabel-crypto-casino-solution">Full Fledged Solution</a>
        @endif
    </div>
@else

<div id='a8r_iframe'></div>
<script>
  //<![CDATA[
    a8rLaunchOptions = { target_element: 'a8r_iframe' };
    a8rLaunchOptions['launch_options'] = {"game_url":"https://bgaming-network.com/play/ElvisFrog/FUN?server=demo","strategy":"detect"};
    window.sg.launch(a8rLaunchOptions);
  //]]>

</script>
@endif