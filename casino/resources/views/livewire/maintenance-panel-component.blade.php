    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight dark:text-white">
            {{ __('Maintenance Panel') }}
        </h2>
    </x-slot>
        <div class="max-w-7xl dark:bg-gray-900 dark:text-white mx-auto py-10 sm:px-6 lg:px-8">   

        <x-jet-action-section>
            <x-slot name="title">
                <span class="dark:text-white">{{ __('Data Actions') }}</span>
            </x-slot>
            <x-slot name="description">
                {{ __('Several simple data actions.') }}
            </x-slot>
            <x-slot name="content" class="dark:bg-gray-700">
                <div class="max-w-xl text-sm text-gray-600">
                    {{ __('It\'s advised to create a backup when running database actions, especially when being in production it is advised to run all these actions on seperate box and then to manually import it to production server.') }}
                </div>
                <div class="mt-5">
                <div class="flex items-center mt-5">
                    <x-jet-button wire:click="clearRawGameslist" wire:loading.attr="disabled">
                        {{ __('Truncate Raw Gameslist') }}
                    </x-jet-button>
                    <x-jet-button class="ml-1" wire:click="clearCache" wire:loading.attr="disabled">
                        {{ __('Clear Cache') }}
                    </x-jet-button>
                    <x-jet-button class="ml-1" wire:click="transferGameImporter" wire:loading.attr="disabled">
                        {{ __('Transfer Raw Games to Regular Gameslist') }}
                    </x-jet-button>
                    <x-jet-button class="ml-1" wire:click="truncateRegularGameslist" wire:loading.attr="disabled">
                        {{ __('Truncate Regular Gameslist') }}
                    </x-jet-button>
                    <x-jet-action-message class="ml-3" on="dataActionCompleted">
                        {{ __('Action completed.') }}
                    </x-jet-action-message>
                </div>
                </div>
            </x-slot>
        </x-jet-action-section>

        <div class="mt-10 mb-10"></div>

        <x-jet-action-section>
            <x-slot name="title">
                <span class="dark:text-white">{{ __('Migrate Fresh') }}</span>
            </x-slot>
            <x-slot name="description">
                {{ __('Drop tables and run database migration.') }}
            </x-slot>
            <x-slot name="content" class="dark:bg-gray-700">
                <div class="max-w-xl text-sm text-gray-600">
                    {{ __('Migration will delete all current data and re-run the migrate, clear cache & seed.') }}
                </div>
                <div class="mt-5">
                <div class="flex items-center mt-5">
                    <x-jet-button wire:click="runFreshMigration" wire:loading.attr="disabled">
                        {{ __('Run Fresh Migration') }}
                    </x-jet-button>
                    <x-jet-action-message class="ml-3" on="freshMigrationDone">
                        {{ __('Migration Completed.') }}
                    </x-jet-action-message>
                </div>
                </div>
            </x-slot>
        </x-jet-action-section>

        <div class="mt-10 mb-10"></div>

        <x-jet-form-section submit="runGameImporter">
            <x-slot name="title">
                 <span class="dark:text-white">{{ __('Game Importer') }}</span>
            </x-slot>

            <x-slot name="description">
                {{ __('Import games from external source. Using Playtech/Evolution game format, you can use many casino as source.') }}
            </x-slot>

            <x-slot name="form">
                <div class="col-span-6 sm:col-span-4">
                <div class="max-w-xl mb-4 text-sm text-gray-600">
                    <p class="mb-2">{{ __('Be advised, that the game importer will launch from the server IP, in many casino\'s gamelist is populated depending on geolocation.') }}</p>
                    <p>{{ __('You can also import the gamelist manually first and save it as a .json file, then upload it somewhere and paste direct link to the .json file below.') }}</p>
                    <p>{{ __('Importer follows the main Evolution/Playtech casino software format.') }}</p>
                </div>                
                    <x-jet-label for="game_importer_link" value="{{ __('Enter gamelist URL:') }}" />
                    <x-jet-input id="game_importer_link" placeholder="For example: https://bets.io/api/games/allowed_desktop" type="text" class="mt-1 text-gray-800 block w-full" wire:model.defer="state.game_importer_link" />
                    <x-jet-input-error for="game_importer_link" class="mt-2" />
                    <x-jet-label for="game_importer_filterkey" value="{{ __('Filter Key') }}" />
                    <x-jet-input id="game_importer_filterkey" placeholder="For example: provider" type="text" class="mt-1 text-gray-800 block w-full" wire:model.defer="state.game_importer_filterkey" />
                    <x-jet-input-error for="game_importer_filterkey" class="mt-2" />
                    <x-jet-label for="game_importer_filtervalue" value="{{ __('Filter Value') }}" />
                    <x-jet-input id="game_importer_filtervalue" placeholder="For example: bgaming" type="text" class="mt-1 text-gray-800 block w-full" wire:model.defer="state.game_importer_filtervalue" />
                    <x-jet-input-error for="game_importer_filtervalue" class="mt-2" />
                </div>
            </x-slot>

            <x-slot name="actions">
                <x-jet-action-message class="mr-3" on="successGameImporter">
                    Imported {{ $countGameImported }} games.
                </x-jet-action-message>
                <x-jet-button wire:loading.attr="disabled">
                        {{ __('Run Game Importer') }}
                 </x-jet-button>
            </x-slot>
            </x-jet-form-section>

            <div class="mt-10 mb-10"></div>


           <livewire:gamesessions-datatable />
        </div>
    </div>