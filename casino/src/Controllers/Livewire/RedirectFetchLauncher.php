<?php

namespace Respins\BaseFunctions\Controllers\Livewire;

use Livewire\Component;
use Respins\BaseFunctions\BaseFunctions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Respins\BaseFunctions\Controllers\API\SessionController;
use Respins\BaseFunctions\Controllers\API\OperatorController;

class RedirectFetchLauncher extends Component
{

    public $url;
    public $error;
    protected $listeners = ['prelauncher_loaded'];

    public function prelauncher_loaded(Request $request)
    {
        $session = $request->entrySession;
        $url = 'https://stage2';
        $this->emit('url', $url);
    }


    
    public function stage_1($starting_url)
    {
        $this->url = $starting_url;
    }

    public static function start($url)
    {
        $init = new RedirectFetchLauncher;
        echo $url;
        $init->stage_1($url);
        $init->render($url);
    }

    public function render()
    {
        return view('respins::livewire.launchers.redirect-fetch-launcher')->layout('respins::layout-extension-livewire-game');
    }


}
