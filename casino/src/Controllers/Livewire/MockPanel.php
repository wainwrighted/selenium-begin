<?php

namespace Respins\BaseFunctions\Controllers\Livewire;

use Livewire\Component;
use Illuminate\Validation\ValidationException;
use Respins\BaseFunctions\Controllers\API\OperatorController;
use Respins\BaseFunctions\Controllers\API\MockController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Respins\BaseFunctions\BaseFunctions;

class MockPanel extends Component
{ 

    public $result;
    public $request_ip;
    public $mock_active_player;

    public $state = [
        'ip' => '',
        'callback_url' => '',
    ];


    public function mount(Request $request)
    {
        $this->request_ip = BaseFunctions::requestIP($request);
        $this->mock_active_player = self::mock_active_player();
    }

    public function message()
    {
        return ;
    }

    public function mock_active_player()
    {
        $get_player = MockController::command_center('activeMockPlayer');
        return $get_player;
    } 
    
    public function generateKey()
    {
        $this->resetErrorBag();
        $ip = $this->state['ip'];
        $link = $this->state['callback_url'];
        $validateIP = filter_var($ip, FILTER_VALIDATE_IP);
        $validateCallbackUrl = filter_var($link, FILTER_VALIDATE_URL);

        if(!$validateIP) {
            throw ValidationException::withMessages([
                'ip' => [__('IP syntax incorrect.')],
            ]);
        }
        if(!$validateCallbackUrl) {
            throw ValidationException::withMessages([
                'callback_url' => [__('Callback URL syntax incorrect.')],
            ]);
        }
        $auth = Auth::user();

        if(!$auth) {
            $this->result = 'Had trouble trying to authenticate your user.';
        }

        $data = [
            'operator_access' => $ip,
            'callback_url' => $link,
            'active' => 1,
            'ownedBy' => $auth->id,
        ];

        $create_key = \Respins\BaseFunctions\Controllers\API\OperatorController::createOperatorKey($data);
        $result = json_decode($create_key, true);
        $this->result = $result['operator_key'];
        $this->emit('generatedKey');
    } 

    public function runGameImporter()
    {
        $this->resetErrorBag();
        $link = $this->state['game_importer_link'];
        $filterkey = $this->state['game_importer_filterkey'] ?? NULL;
        $filtervalue = $this->state['game_importer_filtervalue'] ?? NULL;

        if (filter_var($link, FILTER_VALIDATE_URL)) {
            $launch_job = GameImporterController::game_importer_livewire($link, $filterkey, $filtervalue);
            $launch_job = json_decode($launch_job, true);
            if($launch_job['status'] === 'error') {
                throw ValidationException::withMessages([
                    'game_importer_link' => $launch_job['message'],
                ]);
            } else {
                $this->countGameImported = $launch_job['count_games_imported'];
                $this->emit('successGameImporter');
            }
            
        } else {
            throw ValidationException::withMessages([
                'game_importer_link' => [__('Enter a valid URL.')],
            ]);
        }
    }

    public function render()
    {              
        return view('respins::livewire.mock-control-component')->layout('respins::layout-extension-livewire');
    }
}
