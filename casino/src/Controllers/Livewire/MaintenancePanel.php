<?php

namespace Respins\BaseFunctions\Controllers\Livewire;

use Livewire\Component;
use Illuminate\Http\Request;
use Respins\BaseFunctions\Models\RawGameslist;
use Illuminate\Validation\ValidationException;
use Respins\BaseFunctions\Controllers\Data\GameImporterController;
use Respins\BaseFunctions\Models\Gameslist;

class MaintenancePanel extends Component
{
    public $countGameImported = 0;
    public $thumbnail_prefix;

    public $state = [
        'game_importer_link' => '',
        'game_importer_filterkey' => NULL,
        'game_importer_filtervalue' => NULL,
    ];

    public function mount()
    {
    }

    public function clearRawGameslist()
    {
        $games_count = RawGameslist::truncate();
        $this->emit('dataActionCompleted');
    }

    public function clearCache()
    {
        $cache_clear = \Artisan::call('optimize:clear');
        $this->emit('dataActionCompleted');
    }

    public function runFreshMigration()
    {
        $migrate = \Artisan::call('migrate:fresh');
        $cache_clear = \Artisan::call('cache:clear');
        $this->emit('freshMigrationDone');
    }

    public function truncateRegularGameslist()
    {
        $games_count = Gameslist::truncate();
        $this->emit('dataActionCompleted');
    }

    public function transferGameImporter()
    {
        $launch_job = GameImporterController::game_importer_transfer();
        $this->emit('dataActionCompleted');
    }

    public function runGameImporter()
    {
        $this->resetErrorBag();
        $link = $this->state['game_importer_link'];
        $filterkey = $this->state['game_importer_filterkey'] ?? NULL;
        $filtervalue = $this->state['game_importer_filtervalue'] ?? NULL;

        if (filter_var($link, FILTER_VALIDATE_URL)) { 
            $launch_job = GameImporterController::game_importer_livewire($link, $filterkey, $filtervalue);
            $launch_job = json_decode($launch_job, true);
            if($launch_job['status'] === 'error') {
                throw ValidationException::withMessages([
                    'game_importer_link' => $launch_job['message'],
                ]);
            } else {
                $this->countGameImported = $launch_job['count_games_imported'];
                $this->emit('successGameImporter');
            }
            
        } else {
            throw ValidationException::withMessages([
                'game_importer_link' => [__('Enter a valid URL.')],
            ]);
        }
    }

    public function render()
    {              
        return view('respins::livewire.maintenance-panel-component')->layout('respins::layout-extension-livewire');
    }
}
