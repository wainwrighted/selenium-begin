<?php

namespace Respins\BaseFunctions\Controllers\Livewire\Partials;

use \Respins\BaseFunctions\Models\OperatorAccess;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class OperatorKeysDataTable extends DataTableComponent
{
    protected $model = OperatorAccess::class;
    public function configure(): void
    {
        $this->setPerPageAccepted([25, 50, 100, 200]);
        $this->setPrimaryKey('id');
        $this->setDefaultSort('id', 'desc');
    }

    public function columns(): array
    {
        return [
            Column::make('API Key', 'operator_key')
                ->sortable(),
            Column::make('Access', 'operator_access')
                ->sortable(),
            Column::make('User', 'ownedBy')
                ->sortable(),
        ];
    }
}