<?php

namespace Respins\BaseFunctions\Controllers\Livewire\Partials;

use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Respins\BaseFunctions\Models\Gameslist;
use Rappasoft\LaravelLivewireTables\Views\Columns\BooleanColumn;
use Rappasoft\LaravelLivewireTables\Views\Columns\ImageColumn;
use Rappasoft\LaravelLivewireTables\Views\Columns\SecondaryHeader;
use Rappasoft\LaravelLivewireTables\Views\Filters\SelectFilter;
use Illuminate\Database\Eloquent\Builder;
use Respins\BaseFunctions\Jobs\RetrieveRealDemoURL;
use Rappasoft\LaravelLivewireTables\Views\Columns\ButtonGroupColumn;
use Rappasoft\LaravelLivewireTables\Views\Columns\LinkColumn;

class GamesListDataTable extends DataTableComponent
{
    protected $model = Gameslist::class;
    public function configure(): void
    {
        $this->setPerPageAccepted([25, 50, 100, 200]);
        $this->setPrimaryKey('id')->setTableRowUrl(function($row) {
            return Gameslist::static_link($row->slug);
        })->setSecondaryHeaderTrAttributes(function($rows) {
            return ['class' => 'bg-gray-100'];
        })->setHideBulkActionsWhenEmptyEnabled();
        $this->setDefaultSort('updated_at', 'desc');

    }

    public function columns(): array
    {
        return [       
            ImageColumn::make('#')
                ->location(function($row) {
                    return asset(Gameslist::thumbnail_square($row->gid));
                })
                ->attributes(function($row) {
                    return [
                        'class' => 'w-12 aspect-square rounded-full',
                    ];
                }),
            Column::make('Name', 'name')
                ->searchable()
                ->sortable(),
            Column::make('Provider', 'provider')
                ->searchable()
                ->sortable(),
            Column::make('GID', 'gid'),
            Column::make('Slug', 'slug'),
            Column::make('Type', 'type')
                ->format(
                fn($value, $row, Column $column) => '<small>' .$row->type. '</small>'
                )
                ->html()
                ->sortable(),
            BooleanColumn::make('Demo', 'demoplay')
            ->collapseOnTablet()
            ->sortable(),
            BooleanColumn::make('JP', 'jackpot')
            ->collapseOnTablet()
            ->sortable(),
            BooleanColumn::make('Bonus Buy', 'bonusbuy')
            ->collapseOnTablet()
            ->sortable(),
            BooleanColumn::make('Active', 'enabled')
            ->sortable(),
            Column::make('Source', 'source')
            ->format(
                fn($value, $row, Column $column) => '<small>' .$row->source. '</small>'
            )
            ->html()
            ->sortable(),
            Column::make('ID', 'id')
            ->collapseOnTablet(),
            ButtonGroupColumn::make('Actions')
                ->attributes(function($row) {
                    return [
                        'class' => 'space-x-2',
                    ];
                })
                ->buttons([
                    LinkColumn::make('Play') // make() has no effect in this case but needs to be set anyway
                        ->title(fn($row) => 'Play')
                        ->location(function($row) {
                            return Gameslist::static_link($row->slug);
                        })
                        ->attributes(function($row) {
                            return [
                                'target' => 'https://www.google.com',
                                'class' => 'underline text-blue-500 hover:no-underline',
                            ];
                        }),
                ]),
        ];
    }

    public function bulkActions(): array
    {
        return [
            'enable' => 'Enable Game',
            'disable' => 'Disable Game',
            'delete' => 'Delete Game',
            'origin_url_update' => 'Origin Demo Job',
        ];
    }


    public function origin_url_update()
    {
        $selected_games = GamesList::whereIn('id', $this->getSelected())->get();
        foreach($selected_games as $game) {
        RetrieveRealDemoURL::dispatch($game['gid']);
        }
        $this->clearSelected();
    }

    public function enable()
    {
        GamesList::whereIn('id', $this->getSelected())->update(['enabled' => 1]);
        $this->clearSelected();
    }

    public function disable()
    {
        GamesList::whereIn('id', $this->getSelected())->update(['enabled' => 0]);
        $this->clearSelected();
    }
    
    public function delete()
    {
        GamesList::whereIn('id', $this->getSelected())->delete();
        $this->clearSelected();
    }

    public function filters(): array
    {
        return [
        SelectFilter::make('Active', 'enabled')
        ->setFilterPillTitle('Game Status')
        ->setFilterPillValues([
            '1' => 'Enabled',
            '0' => 'Disabled',
        ])
        ->options([
            '' => 'All',
            '1' => 'Yes',
            '0' => 'No',
        ])
        ->filter(function(Builder $builder, string $value) {
            if ($value === '1') {
                $builder->where('enabled', true);
            } elseif ($value === '0') {
                $builder->where('enabled', false);
            }
        }),

    ];
    }

}