<?php
namespace Respins\BaseFunctions\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Respins\BaseFunctions\Models\Players;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;


class PlayerController
{
	public static function createPlayerFunction($data) {
		$player = new Players();
		$player->player_id = Str::orderedUuid();
		$player->player_operator_id = $data['player_operator_id'];
		$player->operator_key = $data['operator_key'];
		$player->active = true;
		$player->currency = $data['currency'];
		$player->data = $data['data'];
		$player->ownedBy = $data['ownedBy'];
		$player->timestamps = true;
		$player->save();
        return collect($player); 
	}
	public static function findOrCreatePlayer($data)
	{
		$player = self::findPlayerByOperatorId($data['player_operator_id'], $data['currency'], $data['operator_key']);
		if($player === false) {
			$player = self::createPlayerFunction($data);
		}
		$response = array('data' => $player);
		Log::notice($response);
		return $response;
	}

	public static function findPlayerByOperatorId($player_operator_id, $currency, $operator_key)
	{
	    $find = Players::where('player_operator_id', $player_operator_id)->where('operator_key', $operator_key)->first();
		if(!$find) {
			return false;
		}
		$response = array($find);
		return $response;
	}

	public static function findPlayer($player_id)
	{
		$operator_query = Cache::get($player_id);
        if (!$operator_query) {
			$operator_query = Players::where('player_id', $player_id)->first();
			if(!$operator_query) {
				return false;
			} else {
				Cache::put($player_id, $operator_query, now()->addMinutes(60));
			}
		}
		$response = array($findPlayer);
		return $response;
	}
}