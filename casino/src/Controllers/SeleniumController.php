<?php
    namespace Respins\BaseFunctions\Controllers;

    use Laravel\Dusk\Browser;
    use Facebook\WebDriver\Remote\RemoteWebDriver;
    use Facebook\WebDriver\Remote\DesiredCapabilities;
    use Facebook\WebDriver\Chrome\ChromeOptions;

    class SeleniumController extends \Tests\Browser\ExampleTest
    {
        /**
         * A basic browser test example.
         *
         * @return void
         */
        public function test_basic_example()
        {
            $options = (new ChromeOptions)->addArguments(collect([
                $this->shouldStartMaximized() ? '--start-maximized' : '--window-size=1920,1080',
            ])->all());

            $browser = RemoteWebDriver::create(
                $_ENV['DUSK_DRIVER_URL'] ?? 'http://localhost:9515',
                DesiredCapabilities::chrome()->setCapability(
                    ChromeOptions::CAPABILITY, $options
                )
            );

            $get = $browser->get('https://demogamesfree.pragmaticplay.net/gs2c/openGame.do?gameSymbol=vs1024lionsd&jurisdiction=MT&lang=en&lobbyUrl=https://www.n1casino.com/exit_iframe&stylename=sfws_n1casino')->getPageSource();
            dd($get);
            $browser->quit();
        }
    }

