<?php
namespace Respins\BaseFunctions\Controllers\Data;

use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use DB;
use Respins\BaseFunctions\BaseFunctions;
use Respins\BaseFunctions\Traits\ApiResponseHelper;
use Respins\BaseFunctions\Models\RawGameslist;
use Respins\BaseFunctions\Models\Gameslist;
use Respins\BaseFunctions\Jobs\RetrieveRealDemoURL;
use Respins\BaseFunctions\Jobs\BuildExtraMetaGameslist;

class DataJobFunctions
{
    use ApiResponseHelper;

    public static function get_demolink($gid)
    {
        $select = Gameslist::where('gid', $gid)->first();
        $get = Http::timeout(5)->get('https://www.'.$select['source'].$select['origin_demolink']);
        if($get->status() !== 200) {
            $get = Http::timeout(8)->get('https://'.$select['source'].$select['origin_demolink']);
        }
        if($get->status() !== 200) {
            abort(400, 'Error trying to retrieve origin link. Response:'.json_encode($get));
        }

        $origin_demo_launch = BaseFunctions::in_between('{\"game_url\":\"', '\",\"strategy\"', $get);
        $back_slash_removal = BaseFunctions::remove_back_slashes(urldecode($origin_demo_launch)); //remove backslashes
        $final_url = str_replace("u0026", "&", $back_slash_removal);
        $select->update([
            'demolink' => $final_url
        ]);

        $extra_meta = config('gameconfig.'.$select->provider.'.extra_game_metadata');
        if($extra_meta) {
            if($extra_meta !== 0) {
                BuildExtraMetaGameslist::dispatch($gid);
            }
        }
    }

    public static function extra_data_gameslist($gid)
    {
       $select = Gameslist::where('gid', $gid)->first();
       $extra_meta = config('gameconfig.'.$select->provider.'.extra_game_metadata');
        if($extra_meta) {
            $extra_meta::extra_game_metadata($gid);
        }
    }




}