<?php
namespace Respins\BaseFunctions\Jobs;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Respins\BaseFunctions\Controllers\Data\DataJobFunctions;
class BuildExtraMetaGameslist implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private string $gid;

    public function __construct(string $gid)
    {
        $this->gid = $gid;
    }

    public function handle()
    {
        $gid = $this->gid;
        return DataJobFunctions::extra_data_gameslist($gid);
    }
}
