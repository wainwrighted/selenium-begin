## Red Tiger
VIP Lobby Default Game Scaffold
Taken from "VIP Lobby" aka the fraud area in back office:
```
https://cdn.dopamine-gaming.com/stable/games/slots-nsg/80sSpins/
```

```html
<html><head>
    <title>80's Spins</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="google" content="notranslate">
    <meta name="format-detection" content="telephone=no">

    <!-- FavIcons -->
    <link rel="apple-touch-icon" sizes="180x180" href="https://games.maxwingaming.com/platforms/dopamine_next/assets/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://games.maxwingaming.com/platforms/dopamine_next/assets/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://games.maxwingaming.com/platforms/dopamine_next/assets/favicons/favicon-16x16.png">
    <link rel="mask-icon" href="https://games.maxwingaming.com/platforms/dopamine_next/assets/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="https://games.maxwingaming.com/platforms/dopamine_next/assets/favicons/favicon.ico">
    <meta name="theme-color" content="#ffffff">

</head>

<body>
    <script type="text/javascript">
        window.com = window.com || {};
        window.com.casino = window.com.casino || {};
        com.casino.cdn = 'https://games.maxwingaming.com/platforms/dopamine_next/assets/vendor/dopamine/games/';
        com.casino.barsPath = 'https://games.maxwingaming.com/platforms/dopamine_next/assets/vendor/dopamine/games/bars-next/';
        com.casino.bridgePath = 'https://games.maxwingaming.com/platforms/dopamine_next/assets/vendor/dopamine/games/bridge/';
    </script>
    <!-- Import Bridge -->
    <script type="text/javascript" crossorigin="" src="https://games.maxwingaming.com/platforms/dopamine_next/assets/vendor/dopamine/games/bridge/bridge.min.js"></script>
    
    <!-- Import History -->
    <script type="text/javascript" crossorigin="" src="/history/vendor.js"></script>
    <script type="text/javascript" crossorigin="" src="/history/history.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/history/history.min.css">

    <!-- Import Gamble -->
    <!-- <script type="text/javascript" src="https://cdn-eu.cloudedge.info/all/games/slots-nsg/LeprechaunsMagic/../../gamblePanel/vendor.js?t=15907397979348"></script>
    <script type="text/javascript" src="https://cdn-eu.cloudedge.info/all/games/slots-nsg/LeprechaunsMagic/../../gamblePanel/game.min.js?t=15907397979348"></script> -->

    <!-- Import Bars -->
    <script type="text/javascript" crossorigin="" src="https://games.maxwingaming.com/platforms/dopamine_next/assets/vendor/dopamine/games/bars-next/bars.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://games.maxwingaming.com/platforms/dopamine_next/assets/vendor/dopamine/games/bars-next/bars.min.css">

    <!-- Import Game -->

    <script type="text/javascript" src="./vendor.js"></script>
    <script type="text/javascript">
        (function () {
            'use strict';
            var params = com.casino.utils.getURLParams();
            var postParams = [];
            var apiEndpoint = params['api'] && params['api']=='rtg'?'https://gserver-nsg-lab-dev.dopamine-gaming.com/nsg-lab/platform/':'//games.maxwingaming.com/backend/casino/Dummy/';
            params['gameId'] = '80sSpins';
            $('title').text(params['gameId']);
            if (Object.keys(postParams).length > 0) {
                try {
                    window.sessionStorage.setItem('launcherPostParamsCache', JSON.stringify(postParams));
                } catch (e) {
                    // Just to be sure that we wont throw error if otherwise valid game launch will fail.
                }
            } else {
                try {
                    postParams = JSON.parse(window.sessionStorage.getItem('launcherPostParamsCache')) || {};
                } catch (e) {
                    // This is to make sure we don't throw error on empty session storage cache.
                    postParams = {};
                }
            }
            Object.keys(postParams).forEach(function (key){
                params[key] = postParams[key];
            });
            var playMode = params['playMode'] === 'real' ? 'real' : 'demo';
            var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

            com.casino.preconfig = {
                bridge: {
                    feedUrl: 'https://feed-nsg-dev.dopamine-gaming.com',
                    autoStart: true,
                    provider: params['provider'] || 'demoReal',
                    operator: params['operator'] || 'default',
                    timestamp: '?t=1579594798',
                    notifications: {
                        inRealPlay: true,
                        inDemoPlay: true,
                        showUnfinishedWins: true,
                        showUnfinishedNoWins: true
                    },
                    updateGameSession: {
                        enabled: false,
                        updateInterval: 60 // interval in seconds
                    }
                },
                server: {
                    rgsApi: apiEndpoint,
                    launchParams: {
                        gameId: params['gameId'],
                        playMode: playMode,
                        sessionId: params['token'] || '',
                        userData: {
                            lang: params['lang'] || '',
                            affiliate: params['affiliate'] || '',
                            channel: params['channel'] || ''
                        },
                        custom: {
                            siteId: params['siteId'] || '',
                            vertical: params['vertical'] || 'r3_rtp96'
                        }
                    }
                },
                game: {
                    namespace: 'com.nsg.game',
                    preconfig: {
                        // playMode: 'demo', // set by Bridge
                        // lang: 'en', // set by Bridge
                        cdn: '/',
                        delayedBalanceUpdate: false,
                        defaultLang: 'en',
                        splash: true,
                        hideCurrency: isTrue('hideCurrency'),
                        disclaimer: '',
                        skin: params['skin'] || 'next-name-payouts',
                        skinURL: params['skinURL'],
                        gameType: 'slot',
                        gameAppId: params['gameId'],
                        responsive: true,
                        addedAnticipation: params['addedAnticipation'] === 'false' ? false : true,
                        hasSpinProgress: false,
                        minRoundDuration: 0,
                        hasRegulations: false
                    }
                },
                bars: {
                    basePath: 'https://cdn.dopamine-gaming.com/dev/games/slots-nsg/LeprechaunsMagic/../../bars-next/',
                    options: {
                        freeBets: true,
                        history: true,
                        historySrc: 'https://games.maxwingaming.com/apps/history/',
                        roundId: true,
                        fullScreen: true,
                        hasGamble: 'false' === 'true' && 'nsg' === 'cayetano' ? true : false,
                        realPlayButton: isTrue('hasRealPlayButton'),
                        depositButton: Boolean(params['depositURL']),
                        delegateMessages: false,
                        lobby: Boolean(params['lobbyURL']),
                        lobbyURL: params['lobbyURL'] ? decodeURIComponent(params['lobbyURL']) : '',
                        depositURL: params['depositURL'] || '',
                        singleBonus: false,
                        onlyEligibleBonuses: false,
                        hasConcurrentBonuses: true,
                        hasAutoplayRestart: true,
                        hasTurboMode: true,
                        hasTurboSpin: true,
                        autoPlayFields: {
                            totalSpins: true,
                            limitLoss: true,
                            singleWinLimit: true,
                            stopOnJackpot: true,
                            stopOnBonus: true
                        },
                        fixedWidgets: 'nsg' === 'cayetano',
                        inApp: false,
                        hasAutoplayRestart: true,
                        hasWidgets: true,
                        minRoundDuration: 0,
                        hasRegulations: false
                    }
                },
                widgets: {
                    enabled: params['hasWidgets'] !== 'false',
                    enablePanel: params['widgets'] !== 'false',
                    panel: {
                        base: 'https://cdn-eu.cloudedge.info/all/games/slots-nsg/LeprechaunsMagic/../../widgets/panel/',
                        main: 'app.min.js',
                    },
                    tournaments: {
                        enabled: params['hasTournament'] !== 'false',
                        base: 'https://cdn-eu.cloudedge.info/all/games/slots-nsg/LeprechaunsMagic/../../widgets/tournaments-generic/',
                        assets: 'assets/',
                        main: 'app.min.js',
                        leaderboardUrl: 'https://feed-nsg-dev.dopamine-gaming.com/tournaments/leaderboard/',
                    },
                    view: {
                        backgroundColor: 0x004D94
                    },
                    splash: {
                        assets: 'assets/',
                        lang: 'en',
                        waitTime: 0,
                    },
                },
                jackpotPanel: {
                    enabled: params['hasJackpotPanel'] !== 'false',
                    lang: 'en',
                    skin: params['skin'] || 'paddyArcade',
                    basePath: 'https://cdn.dopamine-gaming.com/dev/games/slots-nsg/LeprechaunsMagic/../../../games/widgets/jackpots/network/',
                    main: 'app.js',
                    assets: 'assets/',
                    styles: 'app.css',
                    version: 'jackpotNext'
                },
                gamble: {
                    enabled: false,
                    lang: 'en',
                    basePath: 'https://cdn.dopamine-gaming.com/dev/games/slots-nsg/LeprechaunsMagic/../../gamblePanel/'
                },
                bonusWheel: {
                    cdn: 'https://cdn-eu.cloudedge.info/all/games/slots-nsg/LeprechaunsMagic/',
                    basePath: '../../scenes/bonus-wheels/{skin}/',
                    skin: params['bonusWheelSkin'] || 'base-wheel',
                    enabled: true,
                }
            };
            com.casino.bridge.init(com.casino.preconfig);
            
            function isTrue(name) {
                return params[name] === 'true' || params[name] === '1';
            }

    })();
    </script>
    <script type="text/javascript" src="./require.js" data-main="out"></script>

    <script type="text/javascript" src="./debug.js"></script>



</body></html>
```